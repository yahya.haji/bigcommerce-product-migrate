const axios = require('axios');
const constants = require('../../constants');
const { secondsToHms } = require('../../utils');

const sandboxAxiosInstance = axios.create({
  baseURL: constants.SANDBOX_API_PATH,
  headers: {
    'X-Auth-Token': constants.SANDBOX_ACCESS_TOKEN,
  },
});

const getAllCategories = async ({ rootPath, accessToken, params }) => {
  // return prodCategories;
  const response = await axios.get('/catalog/categories', {
    method: 'GET',
    params,
    baseURL: rootPath,
    headers: {
      'X-Auth-Token': accessToken,
    },
  });
  const data = response.data;
  return data;
};

const deleteAllCategories = async () => {
  // throw Error('If you are sure you want to delete all categories, comment out this line!');
  console.log('Deleting categories...');
  const response = await sandboxAxiosInstance.delete('/catalog/categories', {
    params: {
      'id:greater': '104',
    },
  });
  const data = response.data;
  console.log(data);
  return data;
};

const postNewCategory = async (category) => {
  if (!category) return;
  category.id = undefined;
  const response = await sandboxAxiosInstance.post('/catalog/categories', category);
  return response.data;
};

const copyRecursive = async (parent_id, newParentId, newOldCategoryMap) => {
  let page = 1;
  let totalPages = '?';
  do {
    const categoriesResponse = await getAllCategories({
      rootPath: constants.PROD_API_PATH,
      accessToken: constants.PROD_ACCESS_TOKEN,
      params: {
        parent_id,
        page,
      },
    });

    for (const category of categoriesResponse.data) {
      const oldId = category.id;
      category.id = undefined;
      category.custom_url = category.custom_url || undefined;
      if (newParentId) {
        category.parent_id = newParentId;
      }
      const savedCategory = await postNewCategory(category);
      newOldCategoryMap[oldId] = { id: savedCategory.data.id, name: savedCategory.data.name };
      await copyRecursive(oldId, savedCategory.data.id, newOldCategoryMap);
    }

    totalPages = categoriesResponse.meta.pagination.total_pages;
    page++;
  } while (page <= totalPages);
};

const copyCategories = async () => {
  const newOldCategoryMap = {};

  console.log('Copying Categories...');
  const startTime = Date.now();
  console.log('Start Time: ', new Date(startTime).toLocaleString());

  await copyRecursive(0, undefined, newOldCategoryMap);

  const endTime = Date.now();
  console.log('End Time: ', new Date(endTime).toLocaleString());
  const timeTaken = (endTime - startTime) / 1000;
  const timeTakenFormatted = secondsToHms(timeTaken);
  console.log(`Time taken: ${timeTakenFormatted}`);
  return newOldCategoryMap;
};

module.exports = { copyCategories, getAllCategories, deleteAllCategories };
