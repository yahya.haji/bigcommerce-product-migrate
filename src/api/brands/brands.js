const axios = require('axios');
const constants = require('../../constants');
const { secondsToHms } = require('../../utils');

const sandboxAxiosInstance = axios.create({
  baseURL: constants.SANDBOX_API_PATH,
  headers: {
    'X-Auth-Token': constants.SANDBOX_ACCESS_TOKEN,
  },
});

const getBrand = async ({ rootPath, accessToken, brandId, params }) => {
  const response = await axios.get(`/catalog/brands/${brandId}`, {
    method: 'GET',
    baseURL: rootPath,
    headers: {
      'X-Auth-Token': accessToken,
    },
    params,
  });
  const data = response.data;
  return data;
};

const getAllBrands = async ({ rootPath, accessToken, params }) => {
  const response = await axios.get('/catalog/brands', {
    method: 'GET',
    params,
    baseURL: rootPath,
    headers: {
      'X-Auth-Token': accessToken,
    },
  });
  const data = response.data;
  return data;
};

const deleteAllBrands = async () => {
  console.log('Deleting Brands...');
  const response = await sandboxAxiosInstance.delete('/catalog/brands', {
    params: {
      'id:greater': '0',
    },
  });
  const data = response.data;
  console.log(data);
  return data;
};

const postNewBrand = async (brand) => {
  if (!brand) return;
  brand.id = undefined;
  const response = await sandboxAxiosInstance.post('/catalog/brands', brand);
  return response.data;
};

const copyBrands = async (newOldBrandMap) => {
  let page = 1;
  let totalPages = '?';

  do {
    const brandsResponse = await getAllBrands({
      rootPath: constants.PROD_API_PATH,
      accessToken: constants.PROD_ACCESS_TOKEN,
      params: {
        page,
      },
    });
    console.log(`Got all brands in page: ${page}`);

    for (const brand of brandsResponse.data) {
      const oldId = brand.id;
      brand.id = undefined;
      for (const key in brand) {
        if (brand[key] === null) {
          brand[key] = '';
        }
      }
      const savedBrand = await postNewBrand(brand);
      newOldBrandMap[oldId] = savedBrand.data.id;
    }

    console.log(`Saved brands.`);
    totalPages = brandsResponse.meta.pagination.total_pages;
    page++;
  } while (page <= totalPages);
};

const copyBrandsToSandbox = async () => {
  const newOldBrandMap = {};
  const startTime = Date.now();
  console.log('Start Time: ', new Date(startTime).toLocaleString());

  await copyBrands(newOldBrandMap);

  const endTime = Date.now();
  console.log('End Time: ', new Date(endTime).toLocaleString());
  const timeTaken = (endTime - startTime) / 1000;
  const timeTakenFormatted = secondsToHms(timeTaken);
  console.log(`Time taken: ${timeTakenFormatted}`);

  return newOldBrandMap;
};

module.exports = { getAllBrands, copyBrandsToSandbox, deleteAllBrands, getBrand };
