const axios = require('axios');
const constants = require('../../constants');
const { writeToFile } = require('../../utils');

/**
 * Get All Products that are already in Sandbox
 *
 * @returns {Promise<{[sku: string]: {sku: string, sandboxId: number, prodId: number, }}>}
 */
const getAllProductsInAlreadyInSandbox = async () => {
  const productionSandboxMap = {};
  let page = 1;
  let totalPages = '?';
  const notInProd = [];
  do {
    // Get all products from Sandbox
    const productsInSandox = await getAllProducts({
      rootPath: constants.SANDBOX_API_PATH,
      accessToken: constants.SANDBOX_ACCESS_TOKEN,
      params: {
        page,
        limit: 100,
        include_fields: 'sku,name,id',
      },
    });
    const skus = [...productsInSandox.data?.map((item) => item.sku)];

    // Get all products from production with the same skus
    const allProds = await getAllProducts({
      rootPath: constants.PROD_API_PATH,
      accessToken: constants.PROD_ACCESS_TOKEN,
      params: {
        'sku:in': skus.join(','),
        limit: 100,
        include_fields: 'sku,name,id',
      },
    });

    // Map the products from production to the products from Sandbox
    skus.forEach((sku) => {
      const prodItem = allProds.data.find((prod) => prod.sku == sku);
      const sandboxItem = productsInSandox.data.find((item) => item.sku == sku);
      if (prodItem) {
        productionSandboxMap[sku] = {
          sandboxId: sandboxItem.id,
          sku: sku,
          prodId: prodItem.id,
        };
      } else {
        notInProd.push(sku);
      }
    });
    totalPages = productsInSandox.meta.pagination.total_pages;
    page++;
  } while (page <= totalPages);
  console.log(`There are ${notInProd.length} products that are in Sandbox but not in Production`);
  writeToFile('./out/notInProdButInSandbox.json', JSON.stringify(notInProd));
  return productionSandboxMap;
};

const getProduct = async ({
  rootPath,
  accessToken,
  productId,
  params = {
    include: 'variants,images,custom_fields,bulk_pricing_rules,videos',
  },
}) => {
  const response = await axios.get(`/catalog/products/${productId}`, {
    method: 'GET',
    params,
    baseURL: rootPath,
    headers: {
      'X-Auth-Token': accessToken,
    },
  });
  const data = response.data;
  return data;
};

const getProductBySKU = async ({ rootPath, accessToken, sku, params }) => {
  const response = await axios.get(`/catalog/products`, {
    method: 'GET',
    params: { ...params, sku },
    baseURL: rootPath,
    headers: {
      'X-Auth-Token': accessToken,
    },
  });
  const data = response.data;
  if (!data.data?.length) {
    console.error(`No Product with this SKU: ${sku}`);
    return;
  }
  return data.data[0];
};

const getProductByName = async ({ rootPath, accessToken, name, params }) => {
  const response = await axios.get(`/catalog/products`, {
    method: 'GET',
    params: { ...params, name },
    baseURL: rootPath,
    headers: {
      'X-Auth-Token': accessToken,
    },
  });
  const data = response.data;
  if (!data.data?.length) {
    console.error(`No Product with this SKU: ${sku}`);
    return;
  }
  return data.data[0];
};

const getAllProducts = async ({ rootPath, accessToken, params }) => {
  const response = await axios.get('/catalog/products', {
    method: 'GET',
    params,
    baseURL: rootPath,
    headers: {
      'X-Auth-Token': accessToken,
    },
  });
  const data = response.data;
  return data;
};

const getProductProperties = async (rootPath, accessToken, productId, propertyName, params) => {
  const response = await axios.get(`/catalog/products/${productId}/${propertyName}`, {
    method: 'GET',
    params,
    baseURL: rootPath,
    headers: {
      'X-Auth-Token': accessToken,
    },
  });
  const data = response.data;
  return data;
};

module.exports = {
  getAllProductsInAlreadyInSandbox,
  getProduct,
  getProductBySKU,
  getProductByName,
  getAllProducts,
  getProductProperties,
};
