const axios = require('axios');
const constants = require('../../constants');
const { getProductProperties } = require('./products_getter');

const sandboxAxiosInstance = axios.create({
  baseURL: constants.SANDBOX_API_PATH,
  headers: {
    'X-Auth-Token': constants.SANDBOX_ACCESS_TOKEN,
  },
});

// delete product with id
const deleteProduct = async (productId) => {
  await sandboxAxiosInstance.delete(`/catalog/products/${productId}`);
};

// delete all product properties with a given property name
const deleteProductProperties = async (productId, propertyName) => {
  const propertiesResponse = await getProductProperties(
    constants.SANDBOX_API_PATH,
    constants.SANDBOX_ACCESS_TOKEN,
    productId,
    propertyName
  );
  const properties = propertiesResponse.data;
  for (const property of properties) {
    await deleteProductProperty(productId, propertyName, property.id);
  }
};

const deleteProductProperty = async (productId, propertyName, propertyId) => {
  const response = await sandboxAxiosInstance.delete(`/catalog/products/${productId}/${propertyName}/${propertyId}`);
  return response.data;
};

module.exports = {
  deleteProduct,
  deleteProductProperties,
  deleteProductProperty,
};
