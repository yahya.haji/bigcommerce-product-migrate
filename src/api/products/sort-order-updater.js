const constants = require('../../constants');
const { getAllProductsInAlreadyInSandbox, getProductProperties } = require('./products_getter');
const { deleteProductProperties } = require('./products_deleter');
const { putUpdateProduct } = require('./products_poster');

const updateAllSortOrder = async () => {
  const products = await getAllProductsInAlreadyInSandbox();
  let i = 0;
  for (const sku in products) {
    i++;
    if (!start) continue;
    console.log(`[S] Updating ${sku}`);
    const { sandboxId, prodId } = products[sku];
    // Delete all existing images on the sandbox product
    await deleteProductProperties(sandboxId, 'images');
    console.log('[P] Deleted all images');

    // Get all images from the prod product
    const imagesResponse = await getProductProperties(
      constants.PROD_API_PATH,
      constants.PROD_ACCESS_TOKEN,
      prodId,
      'images'
    );
    const images = imagesResponse.data;
    const unecessaryProperties = [
      'url_zoom',
      'url_thumbnail',
      'url_standard',
      'url_tiny',
      'image_file',
      'date_created',
      'date_modified',
      'product_id',
      'id',
    ];

    // Modify the images to be compatible with the sandbox product
    images.forEach((image) => {
      image.image_url = image.url_standard;

      unecessaryProperties.forEach((prop) => delete image[prop]);
    });

    // Sort the images by sort_order
    const imagesSorted = images.sort((a, b) => a.sort_order - b.sort_order);

    // Post the images to the sandbox product
    await putUpdateProduct({
      product: {
        images: imagesSorted,
      },
      productId: sandboxId,
    });

    console.log(`[F] Updated ${sku} ${i} of ${Object.keys(products).length}`);
  }
};

module.exports = {
  updateAllSortOrder,
};
