const axios = require('axios');
const constants = require('../../constants');

const sandboxAxiosInstance = axios.create({
  baseURL: constants.SANDBOX_API_PATH,
  headers: {
    'X-Auth-Token': constants.SANDBOX_ACCESS_TOKEN,
  },
});

const postNewProductProperty = async (productId, propertyName, data) => {
  const response = await sandboxAxiosInstance.post(`/catalog/products/${productId}/${propertyName}`, data);
  const savedData = response.data;
  return savedData;
};

const putUpdateProductProperty = async ({ productId, propertyName, propertyId, data }) => {
  const response = await sandboxAxiosInstance.put(`/catalog/products/${productId}/${propertyName}/${propertyId}`, data);
  const savedData = response.data;
  return savedData;
};

const postNewProductVariant = async ({ productId, variant }) => {
  const response = await sandboxAxiosInstance.post(`/catalog/products/${productId}/variants`, variant);
  return response.data;
};

const postNewProduct = async (product) => {
  if (!product) return;
  const copy = JSON.parse(JSON.stringify(product));

  copy.id = undefined;
  copy.date_created = undefined;
  copy.date_modified = undefined;
  copy.calculated_price = undefined;
  copy.tax_class_id = undefined;
  copy.base_variant_id = undefined;
  const response = await sandboxAxiosInstance.post('/catalog/products', copy);
  return response.data;
};

const putUpdateProduct = async ({ product, productId, params }) => {
  if (!product) return;
  const copy = JSON.parse(JSON.stringify(product));

  copy.id = undefined;
  copy.date_created = undefined;
  copy.date_modified = undefined;
  copy.calculated_price = undefined;
  copy.base_variant_id = undefined;
  const response = await sandboxAxiosInstance.put(`/catalog/products/${productId}`, copy, {
    params,
  });
  return response.data;
};

const putChannelAssignment = async (data) => {
  if (!data) return;
  const response = await sandboxAxiosInstance.put('/catalog/products/channel-assignments', data);
  return response.data;
};

module.exports = {
  postNewProductProperty,
  postNewProductVariant,
  postNewProduct,
  putUpdateProduct,
  putChannelAssignment,
  putUpdateProductProperty,
};
