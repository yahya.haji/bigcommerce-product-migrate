const constants = require('../../constants');
const { writeToFile, beep, appendLineToFile, secondsToHms } = require('../../utils');

const {
  getAllProductsInAlreadyInSandbox,
  getProduct,
  getAllProducts,
  getProductProperties,
} = require('./products_getter');
const { postNewProductProperty, postNewProduct, putChannelAssignment } = require('./products_poster');
const { deleteProductProperties } = require('./products_deleter');

const MAPPINGS = {};
const ARGUMENTS = {
  VERBOSE: true,
};
const counter = { count: 0 };

function customLog() {
  if (!ARGUMENTS.VERBOSE) return;
  console.log(...arguments);
}

const getFullProduct = async ({ product: _product }) => {
  const product = { ..._product };

  // Map production category ids to sandbox category ids
  product.categories = product.categories.map((oldId) => {
    const newValue = MAPPINGS.categoryIdMap[oldId].id;
    if (newValue === undefined) {
      customLog('Category with id is in prod but not sandbox', oldId);
    }
    return newValue;
  });

  // Map production brand ids to sandbox brand ids
  product.brand_id = MAPPINGS.brandIdMap[product.brand_id];
  product.related_products = [];

  product.custom_fields = product.custom_fields?.map((field) => ({ ...field, id: undefined }));
  product.images = product.images?.map((image) => {
    return {
      id: undefined,
      image_url: image.url_standard,
      is_thumbnail: !!image.is_thumbnail,
      description: image.description || '',
      sort_order: image.sort_order,
    };
  });
  product.videos = product.videos?.map((video) => ({
    ...video,
    id: undefined,
    product_id: undefined,
  }));
  product.bulk_pricing_rules = product.bulk_pricing_rules?.map((rule) => ({ ...rule, id: undefined }));

  // Remove ids from variants and set the product_id to the new product id
  if (product.variants?.length === 1) product.variants = undefined;
  product.variants = product.variants?.map((variant) => {
    variant.id = undefined;
    variant.product_id = undefined;
    return variant;
  });

  return product;
};

const saveProductWithProductId = async ({ productId, alreadyInSandbox, savedProductsIdMap }) => {
  const productResponse = await getProduct({
    rootPath: constants.PROD_API_PATH,
    accessToken: constants.PROD_ACCESS_TOKEN,
    productId,
  });
  const product = productResponse.data;
  return await copyAProduct({ product, alreadyInSandbox, savedProductsIdMap });
};

const addProductModifiers = async ({ oldProductId, newProductId, savedProductsIdMap, alreadyInSandbox }) => {
  const prodModifiersResponse = await getProductProperties(
    constants.PROD_API_PATH,
    constants.PROD_ACCESS_TOKEN,
    oldProductId,
    'modifiers'
  );
  const prodModifiers = prodModifiersResponse.data;
  for (const modifier of prodModifiers) {
    modifier.id = undefined;
    modifier.product_id = newProductId;
    for (const optionValue of modifier.option_values) {
      optionValue.id = undefined;
      optionValue.option_id = undefined;

      if (!/product_list/.test(modifier.type)) continue;
      if (!savedProductsIdMap[optionValue.value_data.product_id]) {
        const savedModifierProd = await saveProductWithProductId({
          productId: optionValue.value_data.product_id,
          alreadyInSandbox,
          savedProductsIdMap,
        });
        customLog(`[${savedModifierProd.name}] has been saved! (Modifier)`);
      }
      optionValue.value_data.product_id = savedProductsIdMap[optionValue.value_data.product_id].sandboxId;
    }

    if (!modifier.option_values?.length) continue;
    modifier.shared_option_id = undefined;

    await postNewProductProperty(newProductId, 'modifiers', modifier);
  }
};

const copyAProduct = async ({ product, savedProductsIdMap, alreadyInSandbox }) => {
  customLog(`Saving ${product?.name}...`);
  const oldProductId = product.id;

  const fullProduct = await getFullProduct({ product, alreadyInSandbox });
  let savedProduct;

  customLog(`${++counter.count}. Creating`, { prodId: oldProductId, sku: fullProduct.sku });
  savedProduct = await postNewProduct(fullProduct);

  const newProductId = savedProduct.data.id;
  savedProductsIdMap[oldProductId] = {
    sandboxId: savedProduct.data.id,
    sku: savedProduct.data.sku,
    prodId: oldProductId,
  };

  // Add Modifiers to Products
  await deleteProductProperties(newProductId, 'modifiers');
  await addProductModifiers({
    oldProductId,
    newProductId,
    savedProductsIdMap,
    alreadyInSandbox,
  });
  return savedProduct.data;
};

const assignToChannels = async () => {
  let page = 1;
  let totalPages = '?';
  do {
    const productsInSandbox = await getAllProducts({
      rootPath: constants.SANDBOX_API_PATH,
      accessToken: constants.SANDBOX_ACCESS_TOKEN,
      params: {
        page,
        limit: 100,
        include_fields: 'id',
      },
    });
    const products = productsInSandbox.data.map((prod) => ({ product_id: prod.id, channel_id: 1 }));
    await putChannelAssignment(products);
    totalPages = productsInSandbox.meta.pagination.total_pages;
    page++;
  } while (page <= totalPages);
};

/**
 *
 * @param {*} newOldProductMap
 * @param {"continue" | "return"} onPageError
 * @param {"continue" | "return"} onProductError
 */
const copyAllProducts = async ({ onPageError, onProductError, savedProductsIdMap, alreadyInSandbox }) => {
  let page = 1;
  let totalPages;
  const productsWithError = [];
  do {
    console.log('Page: ', page, 'Total Pages: ', totalPages);
    try {
      const pageWiseProductMap = {};
      const productsResponse = await getAllProducts({
        rootPath: constants.PROD_API_PATH,
        accessToken: constants.PROD_ACCESS_TOKEN,
        params: {
          page,
          sort: 'id',
          // 'id:greater': 1639, // In cases where you want to start from a specific product
          include: 'variants,images,custom_fields,bulk_pricing_rules,videos',
        },
      });
      customLog(`Got ${productsResponse.meta.pagination.count} products in page: ${page}`);

      for (const product of productsResponse.data) {
        try {
          const oldId = product.id;
          if (savedProductsIdMap[oldId]) {
            customLog(
              `Product [${product.name}] with sku: ${product.sku} and id: ${product.id} has already been copied`
            );
            const itemInSandbox = savedProductsIdMap[oldId];
            pageWiseProductMap[oldId] = { ...itemInSandbox };
            continue;
          }
          const savedProduct = await copyAProduct({
            product,
            savedProductsIdMap,
            alreadyInSandbox,
          });
          customLog(`[${savedProduct.name}] has been saved!`);
          pageWiseProductMap[oldId] = { sandboxId: savedProduct.id, sku: savedProduct.sku, prodId: oldId };
        } catch (productError) {
          // throw productError;
          beep();
          productsWithError.push({ sku: product.sku, prodId: product.id });
          const errorTitle = productError.response?.data?.title;
          const errorStatusCode = productError.response?.status;
          const url = productError.response?.config?.url;
          const method = productError.response?.config?.method;
          const message = `
          sku: ${product.sku}
          prodId: ${product.id}
          errorStatusCode: ${errorStatusCode}
          errorTitle: ${errorTitle}
          url: ${method} ${url}

          ----------------------------------------\n`;
          appendLineToFile('./out/productsWithError.txt', message);
          console.log("[Error] Couldn't save product: ", product.sku, product.id);
          if (onProductError === 'return') {
            throw productError;
          }
        }
      }

      totalPages = productsResponse.meta.pagination.total_pages;
      customLog(`Saved products. Page ${page} out of ${totalPages}`);
      page++;
      if (Object.keys(saveProductWithProductId || {}).length % 50 == 0) {
        console.log(`Copied ${counter.count} products`);
      }
    } catch (pageError) {
      beep();
      console.error({ type: 'Page Error', page, totalPages });
      if (onPageError === 'return') {
        writeToFile('./out/productsWithError.json', JSON.stringify(productsWithError));
        throw pageError;
      }
    }
  } while (page <= totalPages);
  writeToFile('./out/productsWithError.json', JSON.stringify(productsWithError));
};

const copyProductsToSandbox = async ({ categoryIdMap, brandIdMap, folderName }) => {
  if (!categoryIdMap) {
    throw new Error('Category Id Map is required');
  }
  if (!brandIdMap) {
    throw new Error('Brand Id Map is required');
  }
  if (!folderName) {
    throw new Error('Folder Name is required');
  }

  MAPPINGS.categoryIdMap = categoryIdMap;
  MAPPINGS.brandIdMap = brandIdMap;
  Object.freeze(MAPPINGS);
  const savedProductsIdMap = {};

  try {
    customLog('Creating list of already created products...');
    const alreadyInSandbox = await getAllProductsInAlreadyInSandbox();
    writeToFile(`./idMaps/${folderName}/alreadyInSandbox.json`, JSON.stringify(alreadyInSandbox));
    customLog('---Created list of already posted products!---');

    Object.keys(alreadyInSandbox).forEach((key) => {
      const item = alreadyInSandbox[key];
      savedProductsIdMap[item.prodId] = { ...item };
    });

    const startTime = Date.now();
    console.log('Start Time: ', new Date(startTime).toLocaleString());

    customLog('Copying products to sandbox');
    await copyAllProducts({
      onPageError: 'return',
      onProductError: 'return',
      savedProductsIdMap,
      alreadyInSandbox,
    });
    customLog('---Copied products to sandbox---');

    customLog('Assigning to channels');
    await assignToChannels();
    customLog('---Assigned to channels---');

    const endTime = Date.now();
    console.log('End Time: ', new Date(endTime).toLocaleString());
    const timeTaken = (endTime - startTime) / 1000;
    const timeTakenFormatted = secondsToHms(timeTaken);
    console.log(`Time taken: ${timeTakenFormatted}`);
  } catch (error) {
    if (error.config?.data) {
      error.config.data = JSON.parse(error.config.data);
    }
    writeToFile('./out/stderror.json', JSON.stringify(error));

    console.log("There has been an error. Check 'stderror.json' for more details");
    throw error;
  }
  return savedProductsIdMap;
};

module.exports = {
  getAllProducts,
  copyProductsToSandbox,
  getProduct,
  getProductProperties,
  assignToChannels,
};
