const { getProductProperties, getAllProductsInAlreadyInSandbox } = require('./products_getter');
const constants = require('../../constants');
const { putUpdateProductProperty } = require('./products_poster');
const { writeToFile } = require('../../utils');

const updateOptionTypes = async () => {
  const products = await getAllProductsInAlreadyInSandbox();
  const allErrors = {};
  let i = 0;
  // let start = true;
  for (const sku in products) {
    i++;
    // if (sku === 'LD005') start = true;
    // if (!start) continue;
    const { sandboxId, prodId } = products[sku];
    console.log(`[S] Updating ${sku}`);
    const errors = await applyUpdate(prodId, sandboxId);
    if (errors.length) {
      allErrors[sku] = errors;
      console.log(`[F] Error while updating ${sku} ${i} of ${Object.keys(products).length}`);
    } else {
      console.log(`[F] Updated ${sku} ${i} of ${Object.keys(products).length}`);
    }
  }
  writeToFile('option-errors.json', JSON.stringify(allErrors));
};

const applyUpdate = async (prodId, sandboxId) => {
  const optionsWithErrors = [];
  //   const prodId = 1203;
  const prodOptionsResponse = await getProductProperties(
    constants.PROD_API_PATH,
    constants.PROD_ACCESS_TOKEN,
    prodId,
    'options'
  );
  const prodOptions = prodOptionsResponse.data;

  //   const sandboxId = 2329;
  const sandboxOptionsResponse = await getProductProperties(
    constants.SANDBOX_API_PATH,
    constants.SANDBOX_ACCESS_TOKEN,
    sandboxId,
    'options'
  );
  const sandboxOptions = sandboxOptionsResponse.data;

  for (const sandboxOption of sandboxOptions) {
    const prodOption = prodOptions.find((option) => option.display_name === sandboxOption.display_name);
    if (!prodOption) {
      console.log(
        `[!] No option found for ${sandboxOption.display_name} on ProdId: ${prodId} and SandboxId: ${sandboxId}`
      );
      continue;
    }

    sandboxOption.sort_order = prodOption.sort_order;
    sandboxOption.type = prodOption.type;

    const updateOptions = {
      sort_order: sandboxOption.sort_order,
      type: sandboxOption.type,
    };
    try {
      await putUpdateProductProperty({
        productId: sandboxId,
        propertyName: 'options',
        propertyId: sandboxOption.id,
        data: updateOptions,
      });
    } catch (error) {
      optionsWithErrors.push({
        option: sandboxOption,
        error: error.response.data,
        sandboxId,
        prodId,
      });
    }
  }
  return optionsWithErrors;
};

module.exports = {
  updateOptionTypes,
};
