const process = require('process');
require('dotenv').config();

// remove trailing slash
const API_PATH = process.env.API_PATH.replace(/\/$/, '');

// Production Store Hash and Access Token - Source Store
const PROD_STORE_HASH = process.env.PRODUCTION_STORE_HASH;
const PROD_ACCESS_TOKEN = process.env.PRODUCTION_ACCESS_TOKEN;
const PROD_API_PATH = `${API_PATH}/${PROD_STORE_HASH}/v3`;

// Sandbox Store Hash and Access Token - Destination Store
const SANDBOX_STORE_HASH = process.env.SANDBOX_STORE_HASH;
const SANDBOX_ACCESS_TOKEN = process.env.SANDBOX_ACCESS_TOKEN;
const SANDBOX_API_PATH = `${API_PATH}/${SANDBOX_STORE_HASH}/v3`;

module.exports = {
  API_PATH,

  PROD_ACCESS_TOKEN,
  PROD_STORE_HASH,
  PROD_API_PATH,

  SANDBOX_STORE_HASH,
  SANDBOX_ACCESS_TOKEN,
  SANDBOX_API_PATH,
};
