const fs = require('fs');
const path = require('path');

function createFolder(folderPath) {
  // check if the folder exists
  if (!fs.existsSync(folderPath)) {
    // create the folder and its subdirectories recursively
    fs.mkdirSync(folderPath, { recursive: true });
    console.log(`Folder created: ${folderPath}`);
  }
}

const appendLineToFile = (fileName, content) => {
  const folderPath = path.dirname(fileName);
  createFolder(folderPath);
  content = content + '\n';
  fs.appendFileSync(fileName, content, (error) => {
    if (error) {
      console.error('There has been an error while appending to file!');
      return;
    }
    console.log('Appended to file successfully!');
  });
};

const writeToFile = (fileName, content) => {
  const folderPath = path.dirname(fileName);
  createFolder(folderPath);
  fs.writeFileSync(fileName, content, (error) => {
    if (error) {
      console.error('There has been an error while writting to file!');
      return;
    }
    console.log('Written to file successfully!');
  });
};

const readFromFile = (fileName) => {
  return fs.readFileSync(fileName, 'utf-8');
};

const beep = () => {
  console.log('\u0007');
};

const sleep = (ms) => {
  console.log(`Sleeping for ${ms}ms`);
  return new Promise((r) => setTimeout(r, ms));
};

const secondsToHms = (d) => {
  d = Number(d);
  const h = Math.floor(d / 3600);
  const m = Math.floor((d % 3600) / 60);
  const s = Math.floor((d % 3600) % 60);

  const hDisplay = h > 0 ? h + (h == 1 ? ' hour, ' : ' hours, ') : ''; // NOSONAR
  const mDisplay = m > 0 ? m + (m == 1 ? ' minute, ' : ' minutes, ') : ''; // NOSONAR
  const sDisplay = s > 0 ? s + (s == 1 ? ' second' : ' seconds') : ''; // NOSONAR
  return hDisplay + mDisplay + sDisplay;
};

module.exports = { readFromFile, writeToFile, sleep, beep, createFolder, appendLineToFile, secondsToHms };
