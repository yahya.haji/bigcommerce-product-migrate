const brandsAPI = require('./api/brands/brands');
const categoriesAPI = require('./api/categories/categories');
const productsAPI = require('./api/products/products');
const { beep, writeToFile, createFolder } = require('./utils');
const { updateOptionTypes } = require('./api/products/options-updater');
const prompt = require('prompt-sync')();
const constants = require('./constants');

const copyAll = async () => {
  const folderName = new Date().toISOString().replace(/:/g, '-');
  createFolder(`./idMaps/${folderName}`);

  const brandIdMap = await brandsAPI.copyBrandsToSandbox();
  writeToFile(`./idMaps/${folderName}/brandIdMap.json`, JSON.stringify(brandIdMap));
  console.log('Brands Copied');

  const categoryIdMap = await categoriesAPI.copyCategories();
  writeToFile(`./idMaps/${folderName}/categoryIdMap.json`, JSON.stringify(categoryIdMap));
  console.log('Cateogries Copied');
  const savedProductsIdMap = await productsAPI.copyProductsToSandbox({
    brandIdMap,
    categoryIdMap,
    folderName,
  });
  writeToFile(`./idMaps/${folderName}/savedProductsIdMap.json`, JSON.stringify(savedProductsIdMap));
  console.log('Products Copied');
  beep();
};

const updateProductOptionTypes = async () => {
  await updateOptionTypes();
  beep();
};

const testAPIWorking = async () => {
  try {
    await productsAPI.getAllProducts({
      rootPath: constants.SANDBOX_API_PATH,
      accessToken: constants.SANDBOX_ACCESS_TOKEN,
      params: {
        limit: 2,
      },
    });
    console.log("[S] SandBox API's are working fine");
  } catch (err) {
    console.log("[E] Error while testing SandBox API's");
    throw err;
  }
  try {
    await productsAPI.getAllProducts({
      rootPath: constants.PROD_API_PATH,
      accessToken: constants.PROD_ACCESS_TOKEN,
      params: {
        limit: 2,
      },
    });
    console.log("[S] Prod API's are working fine");
  } catch (error) {
    console.log("[E] Error while testing Prod API's");
    throw error;
  }
  return true;
};

const main = async () => {
  try {
    console.log('----------------------------------------');
    console.log('- BigCommerce Product Migration Script -');
    console.log('----------------------------------------\n');
    let response = prompt('Press Enter to test APIs...');
    if (response !== '') {
      console.log('Invalid Input');
      return;
    }
    await testAPIWorking();
    console.log('APIs are working fine');

    // Warn Users that they have to delete all products from sandbox before running this script
    console.log(
      '\n\nNOTE: Make sure to delete all brands, categories, and products from sandbox before running this script'
    );
    response = prompt('Press Enter to start copying...');
    if (response !== '') {
      console.log('Invalid Input');
      return;
    }

    await copyAll();
    console.log('\nDone Copying!');
    console.log('Updating Product Options');
    await updateProductOptionTypes();
    console.log('Done Updating Option Types');
  } catch (error) {
    console.log('There has been an error while copying products, please check the error log file - error.json');
    try {
      if (error.config?.data) {
        error.config.data = JSON.parse(error.config.data);
      }
    } catch (error) {}
    writeToFile('./error.json', JSON.stringify(error));
  }

  prompt('\n\nPress Enter to exit...');
};

main();
