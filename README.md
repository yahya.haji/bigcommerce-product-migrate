# Transfer Products across BigCommerce Stores

> Before executing these script it is better to delete all products, categories, and brands from the destination store first. 

> The best way to delete them is the admin panel itself.

## Steps
After the products, categories, and brands have all been removed, follow the following steps to execute the script.
1. Clone this repo
2. Setup environment file - `.env`
    * Rename the `.env.default` file to `.env`
    * Set up the environment variables
    ```
    # Sample .env file

    PRODUCTION_ACCESS_TOKEN=prod_access_token
    PRODUCTION_STORE_HASH=prod_store_hash

    SANDBOX_ACCESS_TOKEN=sandbox_access_token
    SANDBOX_STORE_HASH=sandbox_store_hash
    ```
3. run `npm install`
4. run `npm start`
